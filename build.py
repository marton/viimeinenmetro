import argparse
import time
import shutil
from pathlib import Path

from hsl_api import (
    get_api_key,
    get_last_departures,
)

from templating import render_main_page


def create_build_dir() -> Path:
    build_dir = Path("_site")
    build_dir.mkdir(exist_ok=True)
    # Clear the build folder if it existed. Don't re-create the
    # whole folder since the file watcher might get messed up.
    for path in build_dir.glob("**/*"):
        if path.is_file():
            path.unlink()
        elif path.is_dir():
            shutil.rmtree(path)
    return build_dir


def parse_args():
    arg_parser = argparse.ArgumentParser(
        description="""
        Build script for viimeinenmetro website.

        HSL API key should be either in the environment variable HSL_API_KEY or in the file HSL_API_KEY next to this script.
        """
    )
    cache_default_fname = "departures.json"
    arg_parser.add_argument(
        "--cache",
        default=None,
        nargs="?",
        const=cache_default_fname,
        metavar="FILE",
        help=f"If given, cache API query results to this file (default: '{cache_default_fname}' if provided without a value)",
    )
    return arg_parser.parse_args()


if __name__ == "__main__":
    t = time.time()
    args = parse_args()
    api_key = get_api_key()
    departures = get_last_departures(
        api_key=api_key,
        cache_fname=args.cache,
    )

    build_dir = create_build_dir()

    for lang, path in [
        ("fi", build_dir),
        ("en", build_dir / "en"),
        ("sv", build_dir / "sv"),
    ]:
        path.mkdir(exist_ok=True)
        with open(path / "index.html", "w") as f:
            f.write(render_main_page(departures=departures, lang=lang))  # type: ignore[arg-type]

    for fname in Path("favicon").glob("**/*"):
        shutil.copy2(fname, build_dir)

    for fname in Path("og-image").glob("**/*.png"):
        shutil.copy2(fname, build_dir)

    print(f"Finished build in {time.time() - t:.2f} seconds")
