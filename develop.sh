#!/bin/bash
set -euxo pipefail

# While developing, use this script to automatically rebuild the site upon
# changes using `watchexec`

# spawn all processes in a process group, so that they all get killed if there's an error
# https://unix.stackexchange.com/questions/179130/if-any-spawned-child-processes-fail-kill-all-and-exit
# see also https://stackoverflow.com/questions/360201/how-do-i-kill-background-processes-jobs-when-my-shell-script-exits
set -m
(

# install with cargo install live-server
# (should use version > 0.6.2 to have the --open option and for it to work properly with the build script)
(live-server --host 127.0.0.1 --open _site || kill 0) &
# install with cargo install watchexec
watchexec -e py,html,htmlt,css,json -r -- python3 build.py --cache
)
