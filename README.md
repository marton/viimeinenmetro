# Viimeinen metro website

This repository contains the source code for the [Viimeinen metro](https://viimeinenmet.ro) website, which shows when the last train departs from each station of the Helsinki metro.


## Building & development

The website is generated from files under the [templates](templates) folder.
The site is built using the [build.py](build.py) build script, which places the generated HTML in the folder `_site`.
The build script has no other dependencies than plain Python 3.8.

To build the site, the build script queries the [Digitransit API](https://digitransit.fi/en/developers/) for today's metro departure times.
The API key for making this query should be stored in the environment variable `HSL_API_KEY` or in a file called `HSL_API_KEY` next to the build script.
While developing, you can pass the `--cache` flag to the build script, which will then save the query result to a JSON file, and read the data from that instead of re-querying it every time.
The cached data is re-queried if it is not from today.


While developing the site, you can enable live-reloading by installing [watchexec](https://watchexec.github.io/) and [live-server](https://crates.io/crates/live-server), and running the script `develop.sh` which launches these two tools.


## Hosting & deployment

The website is hosted on [statichost.eu](https://www.statichost.eu).
The build is re-triggered every day at 05:00 using [cron-job.org](https://cron-job.org), as well as whenever a new push is made to this repository.
The API key is configured in the environment variables of the build environment on statichost.eu.


## License

Code is copyright András Gunyhó, licensed under EUPL v1.2, see [LICENSE](LICENSE) file.
The website content is copyright András Gunyhó, licensed under [CC-BY-NC 4.0](https://www.creativecommons.org/licenses/by-nc/4.0/).

