"""
Functions for HTML templating.

You don't need 20 megabytes of hugo to do it.
"""
from typing import Literal, Dict
import re
import datetime
from zoneinfo import ZoneInfo

import translation as T

from hsl_api import get_station_ids


Lang = Literal["fi", "sv", "en"]


def render_template(template: str, **kwargs) -> str:
    """
    Find double brackets inside a template string and replace them with
    content from kwargs.
    """
    used_keys = set()

    def replacer(m: re.Match):
        template_key = m.group(1)
        if template_key not in kwargs:
            raise ValueError(
                f"Don't know what to do with template key '{template_key}'"
            )
        used_keys.add(template_key)
        return kwargs[template_key]

    result = re.sub(r"\{\{(.*?)}}", replacer, template)
    if unused_keys := set(kwargs.keys()) - used_keys:
        raise ValueError(f"Keys {unused_keys} were not used")
    return result


def render_template_from_file(fname: str, **kwargs) -> str:
    """
    Load the template from fname and render it using kwargs,
    see render_template
    """
    with open(fname, "r") as f:
        try:
            return render_template(f.read(), **kwargs)
        except ValueError as e:
            raise ValueError(f"Error processing template file '{fname}': {e}")


def relative_url_for_lang(lang: Lang) -> str:
    if lang == "fi":
        return "/"
    else:
        return f"/{lang}/"


def render_alternate_lang_tags():
    site_url = "https://viimeinenmet.ro"
    return (
        "\n".join(
            [
                f'<link rel="alternate" hreflang="{lang}" href="{site_url}{relative_url_for_lang(lang)}" />'
                for lang in ["fi", "sv", "en"]
            ]
        )
        + f'\n<link rel="alternate" hreflang="x-default" href="{site_url}/" />'
    )


def render_navbar(lang: Lang) -> str:
    navbar_links = []
    for l in ["fi", "sv", "en"]:
        if l != lang:
            navbar_links.append(f'<a href="{relative_url_for_lang(l)}">{l.upper()}</a>')  # type: ignore[arg-type]
        else:
            navbar_links.append(f"<strong>{l.upper()}</strong>")
    navbar_list_html = "\n".join(f"<li>{l}</li>" for l in navbar_links)
    return f"<nav><ul>{navbar_list_html}</ul></nav>"


def render_nearest_station_finding(lang: Lang) -> str:
    f = T.NearestStationFinding
    return render_template_from_file(
        "templates/nearest-station-finding.html",
        button_label=f.button_label[lang],
        searching=f.searching[lang],
        denied=f.denied[lang],
        unavailable=f.unavailable[lang],
        not_saved_info=f.not_saved_info[lang],
    )


def render_station_info(
    station_name: str,
    lang: Lang,
    departures: Dict[str, datetime.datetime],
    station_ids: Dict[str, str],
    latitude: float,
    longitude: float,
) -> str:
    """
    Render the info for a single station

    Args:
        station_name: Translated name of the station
        lang: The language used for translation (to determine HSL link)
        departures: Dict of the form {terminus_name_fi: departure_time, ...}
        station_ids: Dict of the form {"gtfsId_east": ..., "gtfsId_west": ...}
            which tells the ID's of the station corresponding to east & westbound
            trains, for HSL linking.
    """
    terminus_arrow_fnames = {
        "Kivenlahti": "templates/arrow-up.svg",
        "Vuosaari": "templates/arrow-down.svg",
        "Mellunmäki": "templates/arrow-down.svg",
    }
    terminus_directions = {
        "Kivenlahti": "west",
        "Vuosaari": "east",
        "Mellunmäki": "east",
    }
    departure_info_rows = []
    for terminus_name_translations in T.termini:
        terminus_name = terminus_name_translations[lang]
        if terminus_name in T.station_abbreviations:
            terminus_name = (
                f'<span class="terminus-label-full">{terminus_name}</span>'
                # this will be un-hidden using javascript if applicable
                f'<span class="terminus-label-abbreviated" aria-label="{terminus_name}" style="display: none;">{T.station_abbreviations[terminus_name]}</span>'
            )
        terminus_name_fi = terminus_name_translations["fi"]
        # The termini also have a "departure" to themselves, which is actually the arrival time.
        # For now, we're just skipping those.
        if terminus_name_fi in departures and terminus_name != station_name:
            terminus_gtfsid = station_ids[
                f"gtfsId_{terminus_directions[terminus_name_fi]}"
            ]
            departure_info_rows.append(
                render_template_from_file(
                    fname="templates/departure-info-row.html",
                    terminus_arrow=open(
                        terminus_arrow_fnames[terminus_name_fi], "r"
                    ).read(),
                    terminus_name=terminus_name,
                    departure_live_link=f"https://reittiopas.hsl.fi/{lang}/pysakit/{terminus_gtfsid}",
                    departure_time=departures[terminus_name_fi].strftime("%H:%M"),
                    departure_datetime=departures[terminus_name_fi].isoformat(),
                )
            )
    departure_info_rows_html = "\n".join(departure_info_rows)
    return render_template_from_file(
        fname="templates/station-info.html",
        station_name=station_name,
        # NOTE: not replacing spaces or ö or ä or spaces or anything
        station_anchor=station_name,
        departure_info_rows=departure_info_rows_html,
        latitude=str(latitude),
        longitude=str(longitude),
    )


def render_faq_items(
    lang: Lang,
):
    faq_items = []
    for item in T.faq_content:
        try:
            faq_items.append(
                render_template_from_file(
                    fname="templates/faq-item.html",
                    item_title=item[lang]["title"],
                    item_content=item[lang]["content"],
                    possible_id=f'id="{item[lang]["id"]}"' if "id" in item[lang] else "",
                )
            )
        except KeyError:
            raise KeyError(f"Missing {lang} translation for FAQ item {item}")

        # Swedish only has a single entry, for the others we expect all items to be translated.
        if lang == "sv":
            break

    return "\n".join(faq_items)


def render_main_page(
    departures: Dict[str, Dict[str, datetime.datetime]],
    lang: Lang,
) -> str:
    """
    Args:
        departures: A dict of the form {station_name: { terminus_name_fi: departure_time, ... }, ...}
        lang: Translation language
    """

    station_ids = get_station_ids()

    svg_circle_radius = "10"
    svg_circle_x = "15"
    svg_circle_x2 = str(int(svg_circle_x) + 25)
    stations_mid = [
        render_template_from_file(
            fname="templates/station-mid.html",
            station_info=render_station_info(
                station_name=station_name_translations[lang],
                lang=lang,
                departures=departures[station_name_translations["fi"]],
                station_ids=station_ids[station_name_translations["fi"]],
                latitude=station_ids[station_name_translations["fi"]]["lat"],
                longitude=station_ids[station_name_translations["fi"]]["lon"],
            ),
            svg_circle_radius=svg_circle_radius,
            svg_circle_x=svg_circle_x,
            svg_viewbox_height="150"
        )
        for station_name_translations in T.metro_stations_kivenlahti_itakeskus[1:]
    ]
    stations_fork_mid = [
        render_template_from_file(
            fname="templates/station-fork-mid.html",
            station_info=render_station_info(
                station_name=station_name_translations[lang],
                lang=lang,
                departures=departures[station_name_translations["fi"]],
                station_ids=station_ids[station_name_translations["fi"]],
                latitude=station_ids[station_name_translations["fi"]]["lat"],
                longitude=station_ids[station_name_translations["fi"]]["lon"],
            ),
            svg_circle_radius=svg_circle_radius,
            svg_circle_x=svg_circle_x,
            svg_circle_x2=svg_circle_x2,
        )
        for station_name_translations in T.metro_stations_mellunmaki[:-1]
    ]
    stations_vuosaari_mid = [
        render_template_from_file(
            fname="templates/station-mid.html",
            station_info=render_station_info(
                station_name=station_name_translations[lang],
                lang=lang,
                departures=departures[station_name_translations["fi"]],
                station_ids=station_ids[station_name_translations["fi"]],
                latitude=station_ids[station_name_translations["fi"]]["lat"],
                longitude=station_ids[station_name_translations["fi"]]["lon"],
            ),
            svg_circle_radius=svg_circle_radius,
            svg_circle_x=svg_circle_x,
            # smaller height for puotila + rastila since they only have 2 terminuses
            svg_viewbox_height="120",
        )
        for station_name_translations in T.metro_stations_vuosaari[:-1]
    ]

    # The termini have unique station containers, so we need to render
    # the info boxes separately.
    unique_station_infos = dict()
    for station_name_translations in T.termini:
        key = (
            f"{station_name_translations['fi'].lower().replace('ä', 'a')}_station_info"
        )
        unique_station_infos[key] = render_station_info(
            station_name=station_name_translations[lang],
            lang=lang,
            departures=departures[station_name_translations["fi"]],
            station_ids=station_ids[station_name_translations["fi"]],
            latitude=station_ids[station_name_translations["fi"]]["lat"],
            longitude=station_ids[station_name_translations["fi"]]["lon"],
        )

    updated_time = datetime.datetime.now(tz=ZoneInfo("Europe/Helsinki"))
    # dummy time to test error messages about being out of date
    #updated_time = datetime.datetime(2024, 12, 25, 5) #, tz=ZoneInfo("Europe/Helsinki"))
    return render_template_from_file(
        "templates/index.html",
        lang=lang,
        page_url_root="https://viimeinenmet.ro",
        page_url="https://viimeinenmet.ro" + relative_url_for_lang(lang),
        alternate_links=render_alternate_lang_tags(),
        page_description=T.page_description[lang],
        page_title=T.page_title[lang],
        site_name=T.page_title["fi"],
        page_heading=T.page_heading[lang],
        og_image_alt=T.og_image_alt[lang],
        navbar=render_navbar(lang=lang),
        site_out_of_date_message=T.site_out_of_date_message[lang],
        nearest_station_finding=render_nearest_station_finding(lang=lang),

        stations_mid="\n".join(stations_mid),
        stations_fork_mid="\n".join(stations_fork_mid),
        stations_vuosaari_mid="\n".join(stations_vuosaari_mid),
        **unique_station_infos,

        svg_circle_radius=svg_circle_radius,
        svg_circle_x=svg_circle_x,
        svg_circle_x2=svg_circle_x2,

        updated_date=updated_time.date().isoformat(),
        updated_date_fmt=T.format_date(dt=updated_time, lang=lang),
        updated_datetime=updated_time.isoformat(),
        updated_time_label=T.updated_time_label[lang],
        updated_time=updated_time.strftime("%Y-%m-%d %H:%M"),

        faq_title=T.faq_title[lang],
        faq_items=render_faq_items(lang=lang),
        copyright_info=T.copyright_info[lang],
        metro_gone_label=T.metro_gone_label[lang],
    )
