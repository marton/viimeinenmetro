"""
This module contains all of the text of the website that is to be translated

Is it unorthodox to put it in a Python file? Yes. Does it work? Yes.

All translation items are just dicts with the language as the key.
Some translations are grouped together as a list of dicts.
"""
from typing import Dict, List, Literal
import datetime
import locale

page_title = {
    "fi": "Viimeinen metro",
    "en": "The Last Metro in Helsinki",
    "sv": "Den sista metron",
}


page_heading = {
    "fi": "Milloin viimeinen metro lähtee?",
    "en": "When does the last metro leave?",
    "sv": "När går den sista metron?",
}

page_description = {
    "fi": "Sivu, joka kertoo milloin päivän viimeinen metro lähtee kultakin pysäkiltä.",
    "en": "A website that shows when the day's last metro in Helsinki leaves from each station.",
    "sv": "En sida som visar när dagens sista metro i Helsinki avgår från varje station.",
}

og_image_alt = {
    "fi": f"Teksti, jossa lukee '{page_heading['fi']}'",
    "en": f"Text that says '{page_heading['en']}'",
    "sv": f"Text som säger '{page_heading['sv']}'",
}


site_out_of_date_message = {
    "fi": """Jokin on to~den~nä~kö~i~ses~ti vialla, sivu on päivitetty viimeksi <span id="site-out-of-date-days-since-update"></span> päivä<span id="site-out-of-date-day-plural">ä</span> sitten. Yritä myö~hem~min uu~del~leen. Voit painaa ai~ka~tau~lus~sa olevaa päätepysäkin nimeä näh~däk~se~si aikataulun HSL:n sivulla.<span id="site-out-of-date-feedback-message" style="display: none;"> Voit myös <a href="#feedback">ottaa yhteyttä ylläpitäjään</a>.</span>""".replace("~", "&shy;"),
    "en": """Something is probably wrong, the site was last updated <span id="site-out-of-date-days-since-update"></span> day<span id="site-out-of-date-day-plural">s</span> ago. Please try again later. You can click on the name of a ter~mi~nus in the timetable to see the actual timetable on the HSL site.<span id="site-out-of-date-feedback-message" style="display: none;"> You can also <a href="#feedback">contact the administrator</a>.</span>""".replace("~", "&shy;"),
    "sv": """Något är för~mod~lig~en fel, sidan uppdaterades senast för <span id="site-out-of-date-days-since-update"></span> dag<span id="site-out-of-date-day-plural">ar</span> sedan. Vänligen försök igen senare. Du kan klicka på namnet på en änd~håll~plats i tidtabellen för att se tidtabellen på HRT:s sida.<span id="site-out-of-date-feedback-message" style="display: none;"> Du kan också <a href="/en/#feedback">kontakta ad~min~istr~at~ören</a>.</span>""".replace("~", "&shy;"),
}


class NearestStationFinding:
    button_label = {
        "fi": "Etsi lähin pysäkki",
        "en": "Find nearest station",
        "sv": "Hitta närmaste station",
    }
    searching = {
        "fi": "Haetaan sijaintia...",
        "en": "Getting location...",
        "sv": "Hämtar plats...",
    }
    denied = {
        "fi": "Paikannus estetty",
        "en": "Location permission denied",
        "sv": "Delning av platsinformation är förbjudet",
    }
    unavailable = {
        "fi": "Sijaintitietoa ei ole saatavilla",
        "en": "Location information unavailable",
        "sv": "Platsinformation är inte tillgänglig",
    }
    not_saved_info = {
        "fi": "Sijaintitietoja ei tallenneta mihinkään.",
        "en": "Location data is not stored anywhere.",
        "sv": "Platsinformation sparas inte någonstans.",
    }


def parse_stations(stations_str: str) -> List[Dict[str, str]]:
    res = []
    for line in stations_str.strip().split("\n"):
        station_name_fi, station_name_se = line.split("|")
        res.append(
            {
                "fi": station_name_fi,
                "sv": station_name_se,
                "en": station_name_fi,
            }
        )
    return res


# All stations as a list of dicts
# Note that the Finnish names are always used as keys in the timetable data.
metro_stations_kivenlahti_itakeskus = parse_stations(
    """
Kivenlahti|Stensvik
Espoonlahti|Esboviken
Soukka|Sökö
Kaitaa|Kaitans
Finnoo|Finno
Matinkylä|Mattby
Niittykumpu|Ängskulla
Urheilupuisto|Idrottsparken
Tapiola|Hagalund
Aalto-yliopisto|Aalto-universitetet
Keilaniemi|Kägeludden
Koivusaari|Björkholmen
Lauttasaari|Drumsö
Ruoholahti|Gräsviken
Kamppi|Kampen
Rautatientori|Järnvägstorget
Helsingin yliopisto|Helsingfors universitet
Hakaniemi|Hagnäs
Sörnäinen|Sörnäs
Kalasatama|Fiskehamnen
Kulosaari|Brändö
Herttoniemi|Hertonäs
Siilitie|Igelkottsvägen
Itäkeskus|Östra centrum
"""
)

# Special English translation for some stations
for station_name_fi, station_name_en in [
    ("Rautatientori", "Central Railway Station"),
    ("Helsingin yliopisto", "University of Helsinki"),
    ("Aalto-yliopisto", "Aalto University"),
]:
    i = next(
        i
        for i, v in enumerate(metro_stations_kivenlahti_itakeskus)
        if v["fi"] == station_name_fi
    )
    metro_stations_kivenlahti_itakeskus[i]["en"] = station_name_en

metro_stations_vuosaari = parse_stations(
    """
Puotila|Botby gård
Rastila|Rastböle
Vuosaari|Nordsjö
"""
)

metro_stations_mellunmaki = parse_stations(
    """
Myllypuro|Kvarnbäcken
Kontula|Gårdsbacka
Mellunmäki|Mellungsbacka
"""
)

# Shorthands for station names to be used if space is too narrow
station_abbreviations = {
    "Mellungsbacka": "Mellungsb.",
}

# For convenience
termini = [
    metro_stations_kivenlahti_itakeskus[0],

    metro_stations_mellunmaki[-1],
    metro_stations_vuosaari[-1],
]

metro_gone_label = {
    "fi": "Mennyt",
    "en": "Gone",
    "sv": "Borta",
}

def format_date(
    dt: datetime.datetime,
    lang: Literal["fi", "sv", "en"],
) -> str:
    """
    Can't use Python's built-in locale support on the server,
    have to format dates manually.

    Also, I couldn't find a locale-specific day-month format
    (without year), so that would have to be done manually anyway.
    """
    d = dt.day
    wd = {
        "fi": "Ma Ti Ke To Pe La Su",
        "en": "Mon Tue Wed Thu Fri Sat Sun",
        "sv": "Mån Tis Ons Tor Fre Lör Sön",
    }[lang].split(" ")[dt.weekday()]
    if lang == "fi" or lang == "sv":
        m = dt.month
        res = f"{wd} {d}.{m}."
    elif lang == "en" or lang == "sv":
        # Let's not assume anything about the locale on the server
        # and do this manually as well...
        m = [
            "Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
        ][dt.month - 1]
        res = f"{wd} {m} {d}"
    else:
        raise ValueError(f"invalid language {lang}")
    locale.setlocale(locale.LC_TIME, "")
    return res


updated_time_label = {
    "fi": "Päivitetty",
    "en": "Updated",
    "sv": "Uppdaterad",
}

faq_title = {
    "fi": "UKK",
    "en": "FAQ",
    "sv": "SPOFF",
}

# The FAQ content is stored as a list-of-dicts so that the translations are close to each other.
_repo_url = "https://codeberg.org/marton/viimeinenmetro"
faq_content = [
    {
        "en": {
            "title": "What?",
            "content": "This is a website that shows when today's last train departs from each station of the Helsinki metro. That's it.",
        },
        "fi": {
            "title": "Mitä?",
            "content": "Tämä verkkosivu kertoo, milloin tämän päivän viimeinen metro lähtee kultakin pysäkiltä. Siinä kaikki.",
        },
        "sv": {
            "title": "Vad?",
            "content": 'Se den <a href="/">finska</a> eller <a href="/en">engelska</a> versionen för mer information.',
        },
    },
    {
        "en": {
            "title": "Why?",
            "content": "This information is surprisingly hard to find on the HSL website. I have struggled with this problem for years, and finally decided to create this page. It was also a nice exercise in web design.",
        },
        "fi": {
            "title": "Miksi?",
            "content": "Tätä tietoa on yllättävän vaikeaa löytää HSL:n verkkosivuilta. Olen usean vuoden ajan kärsinyt tästä, ja päätin lopulta tehdä tämän sivun. Tämä oli myös mukava tekosyy harjoitella web designia.",
        },
    },
    {
        "en": {
            "title": "How?",
            "content": f'The departure times are updated every morning at 5:00. <strong>The departure times are entirely based on the HSL scheduled departure times, and delays are not taken into account</strong>. You can click on a departure time or the name of a terminus to see real-time information in the HSL journey planner. The website itself is created with <a href="{_repo_url}">hand-crafted HTML</a>.',
        },
        "fi": {
            "title": "Miten?",
            "content": f'Lähtöajat päivitetään joka aamu klo 5. <strong>Lähtoajat perustuvat täysin HSL:n aikatauluihin, eivätkä ne huomioi mahdollisia viivästyksiä.</strong> Voit näpäyttää lähtöaikaa tai päätepysäkin nimeä nähdäksesi reaaliaikaiset tiedot HSL:n reittioppaassa. Verkkosivu itsessään on tehty käyttäen <a href="{_repo_url}">käsin kirjoitettua HTML:ää</a>.',
        },
    },
    {
        "en": {
            "title": "Who?",
            "content": 'Just <a href="https://marci.gunyho.com" rel="noopener noreferrer">some dude</a> who lives in the Helsinki region. Note that <strong>this website is not officially affiliated with HSL in any way</strong>.',
        },
        "fi": {
            "title": "Kuka?",
            "content": '<a href="https://marci.gunyho.com" rel="noopener noreferrer">Joku tyyppi vaan</a> joka asuu pääkaupunkiseudulla. Huomioithan, että <strong>tämä sivusto ei liity millään lailla virallisesti HSL:ään</strong>.',
        },
    },
    {
        "en": {
            "title": "How can I bookmark my favorite station?",
            "content": 'You can click on the name of any station to link to it, <a href="#Central Railway Station">like this</a>.',
        },
        "fi": {
            "title": "Miten voin lisätä lempipysäkkini kirjanmerkkeihin?",
            "content": 'Voit näpäyttää minkä tahansa pysäkin nimeä linkataksesi siihen, <a href="#Rautatientori">tällä lailla</a>.',
        },
    },
    {
        "en": {"title": "Cookie policy?", "content": "No cookies."},
        "fi": {"title": "Evästekäytännöt?", "content": "Evästeitä ei ole."},
    },
    {
        "en": {
            "title": "Ads?",
            "content": "No. Ads make society worse. This website is so tiny that it has no running costs. It is a public service, for you.",
        },
        "fi": {
            "title": "Mainonta?",
            "content": "Ei ole. Mainokset tekevät yhteiskunnasta huonomman. Tämä sivu on niin pieni, että sillä ei ole juoksevia kuluja. Se on julkinen palvelu, sinua varten.",
        },
    },
    {
        "en": {
            "title": "The website is broken / bad / ugly",
            "content": f'That\'s not a question. But I am very happy to receive feedback or improvement ideas! Feel free to open an issue at <a href="{_repo_url}">the repository</a>, or send <a href="https://marci.gunyho.com" rel="noopener noreferrer">me</a> an email.',
            "id": "feedback",
        },
        "fi": {
            "title": "Sivu on rikki / huono / ruma",
            "content": f'Tuo ei ole kysymys. Mutta palaute ja parannusehdotukset ovat erittäin tervetulleita! Voit luoda issuen <a href="{_repo_url}">repositorioon</a>, tai lähettää <a href="https://marci.gunyho.com" rel="noopener noreferrer">minulle</a> sähköpostia.',
            "id": "feedback",
        },
    },
]

copyright_info = {
    "fi": 'Aikatauludata &copy; HSL on lisensoitu <a href="https://creativecommons.org/licenses/by/4.0/deed.fi">CC BY 4.0</a> -lisenssillä. Sivun muu sisältö &copy; András Gunyhó on lisensoitu <a href="https://creativecommons.org/licenses/by-nc/4.0/deed.fi">CC BY-NC 4.0</a> lisenssillä.',
    "en": 'Timetable data is &copy; HSL licensed under <a href="https://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>. The rest of the content on the page is &copy; András Gunyhó licensed under <a href="https://creativecommons.org/licenses/by-nc/4.0/">CC BY-NC 4.0</a>.',
    "sv": 'Tidtabelssdata är &copy; HSL licenserad under <a href="https://creativecommons.org/licenses/by/4.0/deed.sv">CC BY 4.0</a>. Resten av innehållet på sidan är &copy; András Gunyhó licenserad under <a href="https://creativecommons.org/licenses/by-nc/4.0/deed.sv">CC BY-NC 4.0</a>.',
}
