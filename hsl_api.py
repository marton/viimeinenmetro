"""
Functions for dealing with the HSL API

This module can also be used as a stand-alone script to generate
the metro station ID JSON.
"""
from typing import Dict, Any
from pathlib import Path
from collections import defaultdict
import os
import urllib.request
import json
import datetime
from zoneinfo import ZoneInfo
import sys
import re


def get_api_key() -> str:
    api_key = os.environ.get("HSL_API_KEY", None)
    if api_key is None:
        api_key_fname = "HSL_API_KEY"
        if not Path(api_key_fname).is_file():
            raise ValueError(
                "Please provide the HSL API key in the environment variable HSL_API_KEY or the file HSL_API_KEY"
            )
        with open(api_key_fname, "r") as f:
            api_key = f.read().strip()
    return api_key


def query_hsl_api(
    api_key: str,
    query: str,
    variables: Dict[str, Any] | None = None,
) -> Dict[str, Any]:
    t = datetime.datetime.now()
    ip_address = urllib.request.urlopen("https://ifconfig.me", timeout=10).read().decode("utf-8")
    print(f"Querying HSL API from IP address {ip_address}", file=sys.stderr)

    query_dict: Dict[str, Any] = {
        "query": query.strip(),
    }
    if variables is not None:
        query_dict["variables"] = variables

    req = urllib.request.Request(
        url="https://api.digitransit.fi/routing/v2/hsl/gtfs/v1",
        headers={
            "Cache-Control": "no-cache",
            "digitransit-subscription-key": api_key,
            "Content-Type": "application/json",
        },
        data=bytes(json.dumps(query_dict).encode("utf-8")),
        method="POST",
    )

    response = urllib.request.urlopen(req, timeout=60)
    response_bytes = response.read()
    print(
        f"Fetched {len(response_bytes) * 1e-6} MB of data in {(datetime.datetime.now() - t).total_seconds():.2f} seconds"
    )

    return {
        "query": query,
        "variables": variables,
        "data": json.loads(response_bytes)["data"],
    }


def query_last_departures(api_key: str):
    all_metro_station_ids = []
    with open("metro-station-ids.json", "r") as f:
        for s in json.load(f).values():
            all_metro_station_ids.extend([s["gtfsId_west"], s["gtfsId_east"]])

    query = """
    query StoptimesForStations($stop_ids: [String], $date: String) {
        stops(ids: $stop_ids) {
            gtfsId
            name
            stoptimesForServiceDate(date:$date) {
                # stoptimes might be empty, this helps to identify which ones exactly
                pattern { name }
                stoptimes {
                    scheduledDeparture
                    serviceDay
                    # some additional possibly interesting fields
                    #headsign
                    #scheduledArrival
                    #realtimeDeparture
                    trip
                    {
                        #routeShortName
                        tripHeadsign
                    }
                }
            }
        }
    }
    """
    variables = {
        "stop_ids": all_metro_station_ids,
        "date": datetime.datetime.now().strftime("%Y%m%d"),
        # Query schedule at some other date than today (note: if this is in the past, it will be empty)
        #"date": datetime.datetime(2024, 9, 2, 12, 0, 0).strftime("%Y%m%d"),
    }

    return query_hsl_api(api_key=api_key, query=query, variables=variables)


def normalize_stop_name(station_name: str) -> str:
    # fix "Aalto-yliopisto (M)" to just "Aalto-yliopisto"
    return re.sub(r" \(M\)", "", station_name)


def last_departures_from_all_departures(
    all_departures: Dict,
) -> Dict[str, Dict[str, datetime.datetime]]:
    """
    Given a nested dict containing a query result, return the last departures towards each terminus.
    """
    res: Dict[str, Dict[str, datetime.datetime]] = defaultdict(dict)
    for stop_data in all_departures["data"]["stops"]:
        if stop_data is None:
            continue
        # Find last stoptime for each route going through the given station.
        # This might contain overlapping info, i.e. metro towards Mellunmäki, but also
        # towards "Mellunmäki via Rautatientori"
        last_stoptimes = [
            max(st["stoptimes"], key=lambda x: x["scheduledDeparture"])
            for st in stop_data["stoptimesForServiceDate"]
            # we are querying all routes, some might not have stop times at all
            # (e.g. Rastila-Kivenlahti during summer time(?)), skip empty routes here
            if st["stoptimes"]
        ]
        # Collect stoptimes grouped by tripHeadsign, which will be just Mellunmäki
        # instead of "Mellunmäki via Rautatientori"
        last_departures_grouped = defaultdict(list)
        for st in last_stoptimes:
            # Note: we're not considering Tapiola, since the last metro always goes
            # all the way to Kivenlahti (at least that's what it seems like).
            if st["trip"]["tripHeadsign"] in [
                    "Vuosaari", "Mellunmäki", "Kivenlahti",
            ]:
                last_departures_grouped[st["trip"]["tripHeadsign"]].append(st)
        # Find the actual last departure for each tripHeadsign
        last_departures_merged = {
            k: max(v, key=lambda x: x["scheduledDeparture"])
            for k, v in last_departures_grouped.items()
        }

        stop_name = normalize_stop_name(stop_data["name"])
        for k, v in last_departures_merged.items():
            res[stop_name][k] = datetime.datetime.fromtimestamp(
                v["serviceDay"] + v["scheduledDeparture"]
            ).astimezone(tz=ZoneInfo("Europe/Helsinki"))

            # add manual offset to test countdown
            #res[stop_name][k] -= datetime.timedelta(hours=8)

    return res


def try_get_departures_from_cache(cache_fname: str | None) -> Dict | None:
    if cache_fname is not None:
        if Path(cache_fname).is_file():
            try:
                with open(cache_fname, "r") as f:
                    cached_data = json.load(f)
                # sometimes (e.g. during summer 2024), stop times exist in only
                # some of the departure destinations, so find the first one
                # that has them
                first_stop_nonempty_stoptimes = next(
                    ptn["stoptimes"]
                    for ptn in cached_data["data"]["stops"][0]["stoptimesForServiceDate"]
                    if len(ptn["stoptimes"]) > 0
                )
                cached_service_day = datetime.datetime.fromtimestamp(
                    first_stop_nonempty_stoptimes[0]["serviceDay"]
                ).date()
                if cached_service_day >= datetime.datetime.now().date(): # or True:  # to always use cache while debugging
                    print(f"Successfully loaded data from cache file '{cache_fname}'")
                    return cached_data
                else:
                    print("Cached data is out of date", file=sys.stderr)
            except (KeyError, IndexError, json.decoder.JSONDecodeError, StopIteration) as e:
                print(f"Cached data has unexpected format:", file=sys.stderr)
                import traceback
                traceback.print_exception(e, file=sys.stderr)
        else:
            print("Cache file doesn't exist", file=sys.stderr)

    return None


def get_last_departures(
    api_key: str,
    cache_fname: str | None = None,
) -> Dict[str, Dict[str, datetime.datetime]]:
    """
    Get today's last departures for all stations, possibly from the cache
    or by querying.

    If applicable, update the cache.

    Returns:
        A dict of the form
            {
                station_name: {
                 "Vuosaari": datetime,
                 "Mellunmäki": datetime,
                 "Kivenlahti": datetime,
                },
                ...
            }
    """
    all_departures_data = try_get_departures_from_cache(cache_fname=cache_fname)

    if all_departures_data is None:
        cache_success = False
        all_departures_data = query_last_departures(api_key=api_key)
    else:
        cache_success = True

    if not cache_success and cache_fname is not None:
        print(f"Saved data to cache  file '{cache_fname}'", file=sys.stderr)
        with open(cache_fname, "w") as f:
            json.dump(all_departures_data, f, indent=2, sort_keys=True)

    return last_departures_from_all_departures(all_departures_data)


def get_station_ids() -> Dict[str, Dict[str, str]]:
    """
    Return a dict mapping (normalized) station names to gtfsIds of the corresponding
    east and westbound stations, for linking to reittiopas.
    """
    with open("metro-station-ids.json", "r") as f:
        return {
            normalize_stop_name(station_name): gtfsids
            for station_name, gtfsids in json.load(f).items()
        }


if __name__ == "__main__":
    route_info_fname = "metro-routes-all-stations.json"
    if Path(route_info_fname).exists():
        with open(route_info_fname, "r") as f:
            full_metro_route_info = json.load(f)
        print(f"Loaded full metro route info from '{route_info_fname}'")
    else:
        full_metro_route_info = query_hsl_api(
            api_key=get_api_key(),
            query="""
        {
            r1: route(id: "HSL:31M1") {
                ...routeFields
            }
            r2: route(id: "HSL:31M2") {
                ...routeFields
            }
        }

        fragment routeFields on Route {
            gtfsId
            longName
            shortName
            patterns {
              directionId
              name
              code
              headsign
              stops {
                gtfsId
                name
                lat
                lon
              }
            }
        }
        """,
        )
        with open(route_info_fname, "w") as f:
            json.dump(full_metro_route_info, f, indent=2)

    # The same station may appear in several routes / patterns
    station_ids: Dict[str, Dict[str, str]] = defaultdict(dict)
    for r in full_metro_route_info["data"].values():
        for p in r["patterns"]:
            # dierctionId 0 is east (Vuosaari/Mellu), 1 is west (Kivenlahti/Tapiola)
            direction = ["east", "west"][p["directionId"]]
            for s in p["stops"]:
                k = f"gtfsId_{direction}"
                res_dict = station_ids[s["name"]]
                res_dict[k] = s["gtfsId"]
                # There are separate latitudes & longitudes associated with the east- and westbound stations. To keep
                # things simple, let's just always pick the eastbound station coordinates. (Also, the same station
                # may appear in multiple routes even in the same direction, but that doesn't matter).
                if direction == "east":
                    for coord in ["lat", "lon"]:
                        res_dict[coord] = s[coord]

    with open("metro-station-ids.json", "w") as f:
        json.dump(station_ids, f, indent=2, sort_keys=True)
